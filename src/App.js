import React from 'react';
import Options from './components/Options'
import axios from 'axios'
import './App.scss';

const apiDomain = process.env.REACT_APP_API_ENDPOINT
const apiUrl = `${apiDomain}api/`

class App extends React.Component {
  state = {
    data: [],
    defaultCount: 8
  }

  componentDidMount() {
    console.log(apiUrl)
  }

  getDataFromApi = (count) => {
    if (count) {
      const requestUrl = `${apiUrl}${count}/short/headers`;
      console.log('if count', requestUrl)
      axios.get(requestUrl)
        .then(res => {
          const {data} = res;
          console.log(data)
        })
    }
  }

  changeOptions = (val) => {
    console.log(`resulted val: ${val}`)
    this.getDataFromApi(val)
  }

  render() {
    const {defaultCount} = this.state
    return (
      <div className="app">
        <div className="options">
          <Options
            changeOptions={this.changeOptions}
            defaultCount={defaultCount}
          />
        </div>
        <div className="result">
          here gonna be generated paragrahs
        </div>
      </div>
    )
  }
}

export default App;
