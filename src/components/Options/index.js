import React from 'react';
import { Button, InputNumber, Checkbox } from 'antd';
import {generateRandomNumber} from '../../utils/helper'
import 'antd/dist/antd.css';
import './style.scss';

class Options extends React.Component {
  state = {
    value: null,
    selectedType: 'manual',
    randomNumber: null
  }

  componentDidMount() {
    const { defaultCount } = this.props
    this.setState({
      value: defaultCount
    })
  }

  inputHandler = (value) => this.setState({ value })

  checkboxHandler = (e) => {
    const { randomNumber } = this.state
    const { checked } = e.target
    const selectedType = checked ? 'random' : 'manual'
    const generatedNumber = checked ? generateRandomNumber() : randomNumber
    this.setState({ selectedType, randomNumber: generatedNumber })
  }

  render() {
    const { value, selectedType, randomNumber } = this.state
    const { changeOptions } = this.props
    const isRandom = selectedType === 'random'

    if (changeOptions) {
      const numberOfParagraphs = isRandom ? randomNumber : value
      changeOptions(numberOfParagraphs)
    }

    return (
      <div className="options">
        Number of paragraphs:
        <InputNumber
          min={1}
          value={value}
          onChange={this.inputHandler}
          disabled={isRandom}
        />
        <p><Checkbox onChange={this.checkboxHandler}>Random</Checkbox></p>
      </div>
    )
  }
}

export default Options;
